### Description
Chrome add-on which reverses chronological order of  emails in Gmail (conversation view only).

### Privacy Policy
Add-on doesn’t collect any data.

### Chrome installation
1. Open Chrome
2. Go to: https://chrome.google.com/webstore/detail/gmail-reverse-conversatio/kfgepjmmgamniaefbjlbacahkjjnjoaa
3. Click 'Add to Chrome'
4. Accept necessary permissions

   Plugin needs permissions only to add  css files to mail.google.com. Add-on doesn't add any code (javascript) to Gmail. 
5. Open Gmail site
6. Go to: Settings->General and set Conversation View mode as on.
7. Enjoy!

### Firefox installation
1. Open Firefox
2. Go to: https://addons.mozilla.org/en-GB/firefox/addon/gmail-reverse/
3. Click 'Add to Firefox'
4. Accept necessary permissions

   Plugin needs permissions only to add  css files to mail.google.com. Add-on doesn't add any code (javascript) to Gmail. 
5. Open Gmail site
6. Go to: Settings->General and set Conversation View mode as on.
7. Enjoy!
